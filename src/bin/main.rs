#[macro_use]
extern crate rosrust;

extern crate mech_ros;
use mech_ros::ROS;

fn main() {
  let mut ros = ROS::new();
  ros.init();
  ros.add_publisher();
  ros.send_message();
}