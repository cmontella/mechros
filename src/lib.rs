extern crate mech_core;
extern crate mech_utilities;
#[macro_use]
extern crate rosrust;
#[macro_use]
extern crate lazy_static;
extern crate hashbrown;

use mech_core::{Interner, Transaction};
use mech_core::Value;
use mech_core::{Quantity, ToQuantity, QuantityMath, make_quantity};
use rosrust::{Publisher, Subscriber, Message};
use hashbrown::hash_map::{HashMap, Entry};
use hashbrown::hash_set::HashSet;

mod msg {
    rosrust::rosmsg_include!(std_msgs/String, std_msgs/Bool, sensor_msgs/Image);
}

enum RosMessage {
  Bool(msg::std_msgs::Bool),
  String(msg::std_msgs::String),
}

enum RosPublisher {
  Bool(Publisher<msg::std_msgs::Bool>),
  String(Publisher<msg::std_msgs::String>),
}

impl RosPublisher {
  fn send(&self, msg: &RosMessage)
  {
    match (self, msg) {
      (RosPublisher::Bool(p), RosMessage::Bool(m)) => { p.send(m.clone()); },
      (RosPublisher::String(p), RosMessage::String(m)) => { p.send(m.clone()); },
      _ => (),
    }
  }
}

pub struct ROS {
  publishers: HashMap<String, RosPublisher>,
  subscribers: HashMap<String, Subscriber>,
}

impl ROS {

  pub fn new() -> ROS {
    ROS {
      publishers: HashMap::new(),
      subscribers: HashMap::new(),
    }
  }

  pub fn init(&mut self) {
    rosrust::init("talker");
  }

  pub fn add_publisher(&mut self) {
    let publisher = rosrust::publish("chatter", 100).unwrap();
    self.publishers.insert("Foo".to_string(), RosPublisher::Bool(publisher));
  }

  pub fn send_message(&mut self) {
    
    println!("Sending message");
    let publisher = self.publishers.get(&"Foo".to_string()).unwrap();
    let mut msg = msg::std_msgs::Bool::default();
    msg.data = false;
    publisher.send(&RosMessage::Bool(msg));
    println!("Message sent");
    
  }

}